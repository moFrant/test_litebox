Тестовое задание
================

Зависимости
-----------

* Python3.5
* Django==1.9.8
* django-filter==0.14.0
* djangorestframework==3.4.0
* psycopg2==2.6.2

---

Быстрый старт
=============

1. Создание конфига.
--------------------

Для создания конфига можно возпользоваться скриптом в корне проекта.

`
    python createconfig.py
`

В результате в корне создасться файл *config.json*
В созданном файле все значения проставлены коментариями, нужно их заменить на ваши значения.

2. Проверка.
------------

Чтобы убедиться, что все работает, нужно выполнить следующую команду:

`
    python manage.py check
`

Данная команда проведет анализ конфигурации и проверит наличие всех пакетов для работы проекта.
Если все нормально, то Вы увидете следующее сообщение:

> System check identified no issues (0 silenced).

Дальше можно запустит unit-тесты, которые проверят корректность работы проекта. Для их выполнения необходимо наличие у пользователя базы данных, которого указали в конфиге, наличие прав на создание баз данных.

`
    python manage.py test
`

Если все хорошо, то вы увидите следующее сообщение:
> .....................
> ----------------------------------------------------------------------
> Ran 21 tests in 4.016s
> 
> OK

3. Создание таблиц.
-------------------

`
    python manage.py migrate
`

4. Наполнение базы начальными данными.
--------------------------------------

`
    python manage.py loaddata init.json
`

5. Настройка веб сервера.

Нужно настроить Ваш веб сервер. Файл **WSGI** находится в *test_litebox/wsgi.py*

Так же нужно настроить, чтоб по URL **/static/** перенаправлялся в каталог *static*

Пример конфига для Apache2

>    Listen 80

>    <VirtualHost litebox.wview.ru:80>

>        WSGIScriptAlias / /test_litebox/test_litebox/wsgi.py

>        WSGIPassAuthorization On

>        <Directory /test_litebox/>

>            Require all granted

>        </Directory>

>        WSGIDaemonProcess litebox user=www-data group=www-data home=/test_litebox/ \

>                          processes=2 threads=4 maximum-requests=100 display-name=apache-dw-wsgi

>        WSGIProcessGroup litebox

>        Alias "/static/" "/test_litebox/static/"

>        <Location "/static/">

>            SetHandler None

>        </Location>

>    </VirtualHost>

