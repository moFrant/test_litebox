'''
    Декораторы для режима разработки.
'''

from django.conf        import settings
from django.shortcuts   import redirect
from django.shortcuts   import render_to_response


def debug(function):
    '''
        Декоратор, который закрывает сайт на разработку.
        
        Чтобы использовать необходимо в конфиге наставить следующие настройки:
        
        1) DEBUG = True - включить режим разработки для вывода полных отчетов об ошибках.
        2) DEBUG_TEXT = "Сайт закрыт" - текст сообщения.
        
        :param function function: метод класса, отвечающий за ответ на запрос.
        :return: Возвращает измененный метод класса, если условия выполненные, то возвращает страницу с сообщением о разработке, если не выполнено, то метод работает в штатном режиме.
        :rtype: function
    '''
    
    def _run(self, request, *args, **kwargs):
        _debug = settings.DEBUG
        
        if _debug:
            if not (request.user.is_staff and request.user.is_active):
                _text = settings.DEBUG_TEXT
                
                response = render_to_response('page_debug.htm', {'text': _text}, status=403)
                response.status_code = 403
                return response
                
            else:
                return function(self, request, *args, **kwargs)
                
        else:
            return function(self, request, *args, **kwargs)
    
    return _run
