from datetime import date

class MakeSQL(object):
    '''
        Вспомогательный класс для создания SQL запроса.
        
        Можно задать поля запроса целиком, через задание элемента контейнера с ключом *fields*.
        Значение должно соответствовать списку полей, или значение *__all__* для выбора всех полей.
        
        При создании экземпляра класса изпользуются все поля.
        
        :param str table: название таблицы, для которой создается запрос.
    '''
    def __init__(self, table):
        self.fields         = '__all__'
        self._fields_str    = '*'
        self._filters       = None
        self._filters_str   = None
        self.table          = str(table)
        self._sql           = None
        self._limit         = None
        self._order_by      = None
        
    def __setitem__(self, key, value):
        if key == 'fields':
            if type(value) is str:
                if value == '__all__':
                    self._fields_str    = '*'
                    self.fields        = '__all__'
                    
            elif hasattr(value, '__iter__'):
                self.fields = []
                
                for v in value:
                    self.fields.append(str(v))
                    
                self._fields_str = ', '.join(self.fields)
                
    def __getattr__(self, name):
        if name == 'filters':
            self._make_filters()
            return self._filters_str
            
        elif name == 'sql':
            self._make_sql()
            return self._sql
            
        else:
            return None
    
    def add_filter(self, field, value, operation='=', operator='AND'):
        '''
            Метод, который добавляет фильтр к SQL запросу.
            
            :param str field: название поля
            :param str value: значение поля
            :param str operation: операция сравнения, по умолчанию используется *=*
            :param str operator: логическая операция, используемая для соединения нескольких фильтров, по умолчанию *AND*. Возможные значения *AND, OR*
        '''
        if operator in ('AND', 'OR'):
            if self._filters:
                self._filters.append((
                    field,
                    value,
                    operation,
                    operator,
                    ))
            
            else:
                self._filters = [(
                    field,
                    value,
                    operation,
                    operator,
                    ),]
                    
    def add_limit(self, value):
        '''
            Метод для ограничения выборки.
            
            :param int value: целое число, сколько записей выбрать.
        '''
        try:
            self._limit = str(int(value))
        except:
            pass
            
    def add_order_by(self, value):
        '''
            Задает сортировку.
            
            :param str value: поле, по которому будет идти сортировка. Если первый симфол будет *-*, то сортировка будет идти по возрастанию.
        '''
        if value[0] != '-':
            self._order_by = 'ORDER BY ' + str(value) + ' ASC'
        
        else:
            self._order_by = 'ORDER BY ' + str(value[1:]) + ' DESC'
                    
    def _to_str(self, value):
        if type(value) is str:
            return "'" + value + "'"
            
        elif type(value) is date:
            return "'" + str(value) + "'"
            
        elif type(value) in (list, set, tuple):
            l = []
            for i in value:
                l.append(self._to_str(i))
                
            return '(' + ', '.join(l) + ')'
            
        else:
            return str(value)
                    
    def _make_filters(self):
        '''
            Создание строкового представления фильтра.
        '''
        if self._filters:
            self._filters_str = ''
            for f in self._filters:
                if self._filters_str:
                    self._filters_str += ' ' + f[3] + ' '
                
                if f[2] in ('BETWEEN', 'NOT BETWEEN'):
                    if len(f[1]) == 2:
                        self._filters_str += '(' + str(f[0]) + ' ' + f[2] + ' ' + self._to_str(f[1][0]) + ' AND ' + self._to_str(f[1][1]) + ')'
                
                elif f[2] in ('=', '!=', '>', '<', '>=', '<='):
                    self._filters_str += str(f[0]) + ' ' + f[2] + ' ' + self._to_str(f[1])
                    
                elif (f[2] == 'IN') and (type(f[1]) in (list, set, tuple)):
                    self._filters_str += '(' + str(f[0]) + ' ' + 'IN' + ' ' + self._to_str(f[1]) + ')'
                    
    def _make_sql(self):
        self._sql = 'SELECT ' + self._fields_str + ' FROM ' + self.table
        
        self._make_filters()
        
        if self._filters_str:
            self._sql += ' WHERE ' + self._filters_str
            
        if self._order_by:
            self._sql += ' ' + self._order_by
            
        if self._limit:
            self._sql += ' LIMIT ' + self._limit
        
        self._sql += ';'
            
