from django.contrib                 import admin
from django.contrib.auth.models     import Group
from django.contrib.admin.models    import LogEntry


class InterfaceAdmin(admin.AdminSite):
    site_header         = "Административная панель для тестового задания"
    site_title          = "Django"
    index_title         = "Управление данными"
    empty_value_display = "Нет значения"
    
    
class LogEntryAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'action_time', 'user', 'content_type', 'object_id', 'object_repr', 'action_flag', 'change_message')
    list_filter = ('user', 'content_type')
    date_hierarchy = 'action_time'

    
site = InterfaceAdmin(name="myadmin")

site.register(Group)
site.register(LogEntry, LogEntryAdmin)
