from django.db import models
from django.contrib import admin

class Model(models.Model):
    """
        Базовый класс для моделей данных.
        Добавляет в модели поля дата создания и дата изменения объекта
    """
    date_created  = models.DateTimeField(auto_now_add=True, verbose_name=u'дата создания')
    date_modified = models.DateTimeField(auto_now=True, verbose_name=u'дата изменения')

    class Meta:
        abstract = True
        
        
class ModelAdmin(admin.ModelAdmin):
    """
        Базовый класс для описания представления модели в интерфейсе администратора.
        Делает поля дата создания и дата изменения доступными только для чтения.
    """
    readonly_fields=('date_created', 'date_modified')
