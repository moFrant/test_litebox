'''
    Модели данных приложение Документы.
'''
from django.db.models.signals   import pre_delete
from django.dispatch            import receiver

from core.models                import Model, models

from product.models             import Product
from shop.models                import Shop
from user.models                import User


class TypeDocument(Model):
    '''
        Модель данных Тип документа.
    '''
    name        = models.CharField(
        max_length      = 50,
        verbose_name    = u"название документа",
        )
    
    blocking    = models.BooleanField(
        verbose_name    = u"блокировка изменения документов",
        default         = False,
        )
        
    def __str__(self):
        return self.name
        
    class Meta:
        verbose_name        = u"тип документов"
        verbose_name_plural = u"типы документов"
        
        
class Document(Model):
    '''
        Модель данных Документ.
    '''
    shop    = models.ForeignKey(
        Shop,
        verbose_name    = u"магазин",
        related_name    = 'document_shop',
        )
    
    date    = models.DateField(
        verbose_name    = u"дата документа",
        )
    
    type    = models.ForeignKey(
        TypeDocument,
        verbose_name    = u"тип документа",
        related_name    = 'document_type',
        )
        
    product = models.ManyToManyField(
        Product,
        verbose_name    = u"товары в документе",
        related_name    = 'document_product',
        through         = 'ProductDocument',
        symmetrical = False
        )
    
    user = models.ForeignKey(
        User,
        verbose_name    = u"пользователь",
        related_name    = 'document_user',
        blank           = True,
        null            = True,
        )
        
    def __str__(self):
        return ': '.join((
            str(self.date),
            str(self.type),
            ))
            
    class Meta:
        verbose_name        = u"документ"
        verbose_name_plural = u"документы"
        ordering            = ["-date",]
        
        permissions         = (
            ('admin', u"Права администратора"),
            )
            
@receiver(pre_delete, sender=Document)
def delete_productdocument(sender, **kwargs):
    '''
        Перехватывает сигнал удаления, чтобы удалить записи из промежуточной модели.
    '''
    document    = kwargs['instance']
    d_product   = ProductDocument.objects.filter(document = document)
    
    d_product.delete()
    
    
class ProductDocument(Model):
    '''
        Промежуточная модель для связи Документов и Товаров.
    '''
    product     = models.ForeignKey(
        Product,
        verbose_name    = u"товар",
        related_name    = 'productdocument_product',
        )
        
    count       = models.FloatField(
        verbose_name    = u"количество",
        default         = 1.0,
        )
        
    document    = models.ForeignKey(
        Document,
        related_name = 'productdocument_document',
        )
        
    def __str__(self):
        return ': '.join((
            str(self.product),
            str(round(self.count, 1)),
            ))
