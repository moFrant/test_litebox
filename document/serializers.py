'''
    Сериализаторы приложения Документы
'''
from rest_framework         import serializers

from product.models         import Product
from product.serializers    import ProductSerializer
from user.serializers       import UserSerializer
from shop.serializers       import ShopSerializer

from .models                import TypeDocument, ProductDocument, Document


class TypeDocumentSerializer(serializers.ModelSerializer):
    '''
        Сериализатор Типа документа.
    '''
    class Meta:
        model   = TypeDocument
        fields  = ('id', 'name')
        
        
class ProductDocumentSerializer(serializers.ModelSerializer):
    '''
        Сериализатор промежуточной модели для связи Товаров, количества и Документов.
    '''
    product = ProductSerializer(many=False, read_only=True)
    
    class Meta:
        model   = ProductDocument
        fields  = ('id', 'product', 'count')
        

class AddProductDocumentSerializer(serializers.ModelSerializer):
    '''
        Сериализатор для добавления промежуточной модели для связи Товаров, количества и Документов.
    '''
    class Meta:
        model   = ProductDocument
        fields  = ('id', 'product', 'count', 'document')


class DocumentSerializer(serializers.ModelSerializer):
    '''
        Сериализатор для Документов.
    '''
    type    = TypeDocumentSerializer(many=False, read_only=True)
    product = ProductDocumentSerializer(source='productdocument_document', many=True, read_only=True)
    user    = UserSerializer(many=False, read_only=True)
    shop    = ShopSerializer(many=False, read_only=True)
    
    class Meta:
        model   = Document
        fields  = ('id', 'shop', 'date', 'type', 'product', 'user')
        

class AddDocumentSerializer(serializers.ModelSerializer):
    '''
        Сериализатор для добавления и изменения документов.
    '''
    class Meta:
        model   = Document
        fields  = ('id', 'shop', 'date', 'type')
