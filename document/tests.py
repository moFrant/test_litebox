from django.test                import TestCase
from django.contrib.auth.models import Permission

from rest_framework.test        import APIClient
from rest_framework.reverse     import reverse

from datetime                   import date, timedelta, datetime

from core.models                import models

from product.models             import Product, UnitProduct
from shop.models                import Shop
from user.models                import User

from .models                    import TypeDocument, ProductDocument, Document
from .serializers               import TypeDocumentSerializer, ProductDocumentSerializer, DocumentSerializer

import json


class DocumentTestCase(TestCase):
    '''
        Проверка приложения Документы.
    '''
    def setUp(self):
        self.APIClient = APIClient()
        
        type_add = TypeDocument(
            name    = 'Приходная накладная',
            )
            
        type_add.save()
        
        type_d = TypeDocument(
            name    = 'Расходная накладная',
            blocking= True,
            )
            
        type_d.save()
        
        unit = UnitProduct(
            name    = u"штука",
            abb     = u"шт.",
            comment = u"используется для товаров, которые можно посчитать штуками"
            )
            
        unit.save()
        
        product = Product(
            name    = u"карандаш",
            unit    = unit,
            barcode = 'qwertyuiop',
            )
            
        product.save()
        
        #создание обычного пользователя
        user = User(
            username    = 'user',
            first_name  = u'Иван',
            patronymic  = u'Иванович',
            last_name   = u'Иванов',
            email       = u'd@wview.ru',
            password    = 'Test0001',
            )
        
        user.save()
        
        #создание администратора
        user_ad = User(
            username    = 'admin',
            first_name  = u'Олег',
            patronymic  = u'Петрович',
            last_name   = u'Бабушкин',
            email       = u'mofrant@wview.ru',
            is_staff    = True,
            password    = 'Test0002',
            )
        
        user_ad.save()
        
        shop = Shop(
            name = u"тестовый магазин",
            )
            
        shop.save()
        
        document = Document(
            shop    = shop,
            date    = date.today(),
            type    = type_add,
            user    = user,
            )
            
        document.save()
        
        product_d = ProductDocument(
            product = product,
            count   = 5,
            document= document,
            )
            
        product_d.save()
        
        document = Document(
            shop    = shop,
            date    = date.today(),
            type    = type_d,
            user    = user_ad,
            )
            
        document.save()
        
        product_d = ProductDocument(
            product = product,
            count   = 1,
            document= document,
            )
            
        product_d.save()
        
    def test_models(self):
        '''
            Проверка модели данных.
        '''
        type_d              = TypeDocument.objects.all()[0]
        document            = Document.objects.all()[0]
        product_document    = ProductDocument.objects.all()[0]
        
        self.assertTrue(type(type_d._meta.get_field('name')) is models.CharField)
        self.assertTrue(type(type_d._meta.get_field('blocking')) is models.BooleanField)
        self.assertEqual(type_d._meta.get_field('blocking').default, False)
        
        self.assertTrue(type(product_document._meta.get_field('product')) is models.ForeignKey)
        self.assertTrue(product_document._meta.get_field('product').remote_field.model is Product)
        self.assertEqual(product_document._meta.get_field('product').remote_field.related_name, 'productdocument_product')
        self.assertTrue(type(product_document._meta.get_field('count')) is models.FloatField)
        self.assertEqual(product_document._meta.get_field('count').default, 1.0)
        
        self.assertTrue(type(document._meta.get_field('shop')) is models.ForeignKey)
        self.assertTrue(document._meta.get_field('shop').remote_field.model is Shop)
        self.assertEqual(document._meta.get_field('shop').remote_field.related_name, 'document_shop')
        self.assertTrue(type(document._meta.get_field('date')) is models.DateField)
        self.assertTrue(type(document._meta.get_field('type')) is models.ForeignKey)
        self.assertTrue(document._meta.get_field('type').remote_field.model is TypeDocument)
        self.assertEqual(document._meta.get_field('type').remote_field.related_name, 'document_type')
        self.assertTrue(type(document._meta.get_field('product')) is models.ManyToManyField)
        self.assertTrue(document._meta.get_field('product').remote_field.model is Product)
        self.assertEqual(document._meta.get_field('product').remote_field.related_name, 'document_product')
        self.assertEqual(document._meta.get_field('product').remote_field.through, ProductDocument)
        self.assertTrue(type(document._meta.get_field('user')) is models.ForeignKey)
        self.assertTrue(document._meta.get_field('user').remote_field.model is User)
        self.assertEqual(document._meta.get_field('user').remote_field.related_name, 'document_user')
        
    def test_signals(self):
        '''
            Проверка работы сигнала удаления.
        '''
        productDocument     = ProductDocument.objects.all()
        document            = Document.objects.all()[0]
        
        self.assertEqual(len(productDocument), 2)
        
        document.delete()
        
        productDocument = ProductDocument.objects.all()
        
        self.assertEqual(len(productDocument), 1)
        
    def test_serialziers(self):
        '''
            Проверка сериализаторов приложения.
        '''
        type_document = TypeDocument.objects.all()[0]
        product_document    = ProductDocument.objects.filter(document__type=type_document)[0]
        document = product_document.document
            
        type_serializer = {
            'id':   type_document.pk,
            'name': type_document.name,
            }
            
        self.assertEqual(type_serializer, TypeDocumentSerializer(type_document, many=False).data)
        
        product_serializer = {
            'id':       product_document.pk,
            'product':  {
                    'id':       product_document.product.pk,
                    'name':     product_document.product.name,
                    'unit':     {
                        'id':   product_document.product.unit.pk,
                        'name': product_document.product.unit.name,
                        'abb':  product_document.product.unit.abb,
                        },
                    'barcode':  product_document.product.barcode,
                    },
            'count':    product_document.count,
            }
            
        self.assertEqual(product_serializer, json.loads(json.dumps(ProductDocumentSerializer(product_document, many=False).data)))
        
        document_serializer = {
            'id':   document.pk,
            'shop': {
                'id':   document.shop.pk,
                'name': document.shop.name,
                },
            'date': str(document.date),
            'type': {
                'id':   document.type.pk,
                'name': document.type.name,
                },
            'product':  [{
                'id':       product_document.pk,
                'product':  {
                    'id':       product_document.product.pk,
                    'name':     product_document.product.name,
                    'unit':     {
                        'id':   product_document.product.unit.pk,
                        'name': product_document.product.unit.name,
                        'abb':  product_document.product.unit.abb,
                        },
                    'barcode':  product_document.product.barcode,
                    },
                'count':    product_document.count,
                },],
            'user': {
                'id':           document.user.pk,
                'username':     document.user.username,
                'first_name':   document.user.first_name,
                'patronymic':   document.user.patronymic,
                'last_name':    document.user.last_name,
                },
            }
            
        self.assertEqual(json.loads(json.dumps(DocumentSerializer(document, many=False).data)), document_serializer)
        
    def test_type_view(self):
        '''
            Проверка представлений Типов документов.
        '''
        type_document   = TypeDocument.objects.all()[0]
        type_serializer = {
            'id':   type_document.pk,
            'name': type_document.name,
            }
        
        self.APIClient.login(username='user', password='Test0001')
        
        response = self.APIClient.get(reverse('typedocument-list'))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(type_serializer in response_json)
        
        response = self.APIClient.get(reverse('typedocument-detail', args=[type_document.pk]))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(type_serializer == response_json)
        
    def test_product_view(self):
        '''
            Проверка представления для промежуточной модели.
        '''
        product_document    = ProductDocument.objects.all()[0]
        product_serializer  = {
            'id':       product_document.pk,
            'product':  {
                    'id':       product_document.product.pk,
                    'name':     product_document.product.name,
                    'unit':     {
                        'id':   product_document.product.unit.pk,
                        'name': product_document.product.unit.name,
                        'abb':  product_document.product.unit.abb,
                        },
                    'barcode':  product_document.product.barcode,
                    },
            'count':    product_document.count,
            }
            
        self.APIClient.login(username='user', password='Test0001')
        
        response = self.APIClient.get(reverse('productdocument-list'))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(product_serializer in response_json)
        
        response = self.APIClient.get(reverse('productdocument-detail', args=[product_document.pk,]))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(product_serializer == response_json)
        
    def test_document_get(self):
        '''
            Проверка получения списка документов.
        '''
        product_document    = ProductDocument.objects.filter()[0]
        document            = product_document.document
        document_serializer = {
            'id':   document.pk,
            'shop': {
                'id':   document.shop.pk,
                'name': document.shop.name,
                },
            'date': str(document.date),
            'type': {
                'id':   document.type.pk,
                'name': document.type.name,
                },
            'product':  [{
                'id':       product_document.pk,
                'product':  {
                    'id':       product_document.product.pk,
                    'name':     product_document.product.name,
                    'unit':     {
                        'id':   product_document.product.unit.pk,
                        'name': product_document.product.unit.name,
                        'abb':  product_document.product.unit.abb,
                        },
                    'barcode':  product_document.product.barcode,
                    },
                'count':    product_document.count,
                },],
            'user': {
                'id':           document.user.pk,
                'username':     document.user.username,
                'first_name':   document.user.first_name,
                'patronymic':   document.user.patronymic,
                'last_name':    document.user.last_name,
                },
            }
            
        self.APIClient.login(username='user', password='Test0001')
        
        response = self.APIClient.get(reverse('document-list'))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(document_serializer in response_json)
        
        delta   = timedelta(weeks=1)
        _date   = document.date
        
        #фильтр по периоду дат
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_start':   datetime.strftime(_date - delta, '%d-%m-%Y'),
                'date_end':     datetime.strftime(_date + delta, '%d-%m-%Y'),
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_start':   datetime.strftime(_date + delta, '%d-%m-%Y'),
                'date_end':     datetime.strftime(_date + delta + delta, '%d-%m-%Y'),
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertFalse(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_start':   datetime.strftime(_date + delta, '%Y-%m-%d'),
                'date_end':     datetime.strftime(_date + delta + delta, '%d-%m-%Y'),
                },
            )
        self.assertEqual(response.status_code, 400)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Неверный формат начальной даты")
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_start':   datetime.strftime(_date + delta, '%d-%m-%Y'),
                'date_end':     datetime.strftime(_date + delta + delta, '%Y-%m-%d'),
                },
            )
        self.assertEqual(response.status_code, 400)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Неверный формат конечной даты")
        
        #фильтр по начальной дате
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_start':   datetime.strftime(_date - delta, '%d-%m-%Y'),
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_start':   datetime.strftime(_date + delta, '%d-%m-%Y'),
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertFalse(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_start':   datetime.strftime(_date + delta, '%Y-%m-%d'),
                },
            )
        self.assertEqual(response.status_code, 400)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Неверный формат начальной даты")
        
        #фильтр по конечной дате
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_end':   datetime.strftime(_date + delta, '%d-%m-%Y'),
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_end':   datetime.strftime(_date - delta, '%d-%m-%Y'),
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertFalse(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'date_end':   datetime.strftime(_date + delta, '%Y-%m-%d'),
                },
            )
        self.assertEqual(response.status_code, 400)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Неверный формат конечной даты")
        
        #фильтр по типу документа
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'type':   document.type.pk,
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'type':   'qwer',
                },
            )
        self.assertEqual(response.status_code, 400)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Неверный формат типа документа")
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'type':   400,
                },
            )
        self.assertEqual(response.status_code, 404)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Тип документа не существует")
        
        #фильтр по магазину
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'shop':   document.shop.pk,
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'shop':   'qwer',
                },
            )
        self.assertEqual(response.status_code, 400)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Неверный формат магазина")
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'shop':   400,
                },
            )
        self.assertEqual(response.status_code, 404)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Магазин не существует")
        
        #фильтр по пользователю
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'user':   document.user.pk,
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(document_serializer in response_json)
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'user':   'qwer',
                },
            )
        self.assertEqual(response.status_code, 400)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Неверный формат пользователя")
        
        response = self.APIClient.get(
            reverse('document-list'),
            {
                'user':   400,
                },
            )
        self.assertEqual(response.status_code, 404)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEqual(response_json['detail'], u"Пользователь не существует")
        
    def test_document_user(self):
        '''
            Проверка автоматической записи пользователя при создании/изменения документа.
        '''
        type_document = TypeDocument.objects.filter(blocking=False)[0]
        document = Document.objects.filter(type=type_document)[0]
        
        self.APIClient.login(username='admin', password='Test0002')
        
        response = self.APIClient.post(
            reverse('document-list'),
            {
                'date': date.today(),
                'shop': document.shop.pk,
                'type': type_document.pk,
                },
            )
            
        self.assertEqual(response.status_code, 201)
        
        response_json   = json.loads(response.content.decode('utf-8'))
        document_new    = Document.objects.get(pk=response_json['id'])
        self.assertEqual(document_new.user.username, 'admin')
        
        self.assertEqual(document.user.username, 'user')
        self.assertEqual(document.date, date.today())
        
        response = self.APIClient.patch(
            reverse('document-detail', args=[document.pk,]),
            {
                'date': date.today() - timedelta(days=1),
                },
            )
        self.assertEqual(response.status_code, 200)
            
        response_json   = json.loads(response.content.decode('utf-8'))
        document_new    = Document.objects.get(pk=response_json['id'])
        self.assertEqual(document_new.user.username, 'admin')
        
        product = ProductDocument.objects.filter(document=document)[0]
        
        self.assertNotEqual(document.user.username, 'admin')
        
        response = self.APIClient.patch(
            reverse('productdocument-detail', args=[product.pk,]),
            {
                'count': 10,
                },
            )
        self.assertEqual(response.status_code, 200)
        
        document_new    = Document.objects.get(pk=document.pk)
        self.assertEqual(document_new.user.username, 'admin')
        
    def test_permissons(self):
        '''
            Проверка ограничение прав на определенные объекты
        '''
        permission = Permission.objects.filter(codename='admin')[0]
        user_admin = User.objects.filter(is_staff=True)[0]
        user_admin.user_permissions.add(permission)
        
        type_document_block = TypeDocument.objects.filter(blocking=True)[0]
        type_document       = TypeDocument.objects.filter(blocking=False)[0]
        document            = Document.objects.filter(type=type_document_block)[0]
        product             = ProductDocument.objects.filter(document=document)[0]
        
        self.APIClient.login(username='user', password='Test0001')
        
        response = self.APIClient.patch(
            reverse('document-detail', args=[document.pk,]),
            {
                'date': date.today() - timedelta(days=1),
                },
            )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data['detail'], u"Разрешено только администраторам")
        
        response = self.APIClient.patch(
            reverse('productdocument-detail', args=[product.pk,]),
            {
                'count': 80,
                },
            )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data['detail'], u"Разрешено только администраторам")
        
        self.APIClient.logout()
        
        self.APIClient.login(username='admin', password='Test0002')
        
        response = self.APIClient.patch(
            reverse('document-detail', args=[document.pk,]),
            {
                'date': date.today() - timedelta(days=1),
                },
            )
        self.assertEqual(response.status_code, 200)
        
        response = self.APIClient.patch(
            reverse('productdocument-detail', args=[product.pk,]),
            {
                'count': 80,
                },
            )
        self.assertEqual(response.status_code, 200)
