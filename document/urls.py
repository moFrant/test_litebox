from rest_framework.routers import DefaultRouter

from .views                 import TypeDocumentViewSet, ProductDocumentViewSet, DocumentViewSet


router = DefaultRouter()
router.register(r'types', TypeDocumentViewSet)
router.register(r'products', ProductDocumentViewSet)
router.register(r'documents', DocumentViewSet)

urlpatterns = router.urls
