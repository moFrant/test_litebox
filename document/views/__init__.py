'''
    Представления приложения Докумненты.
'''
from .type_document     import TypeDocumentViewSet
from .product_document  import ProductDocumentViewSet
from .document          import DocumentViewSet
