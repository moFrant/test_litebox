from rest_framework.exceptions  import ParseError, NotFound, PermissionDenied
from rest_framework.response    import Response
from rest_framework             import viewsets, status

from datetime                   import date, datetime

from core.make_sql              import MakeSQL

from shop.models                import Shop
from user.models                import User

from .type_document             import TypeDocumentViewSet

from ..models                   import Document, ProductDocument, TypeDocument
from ..serializers              import DocumentSerializer, AddProductDocumentSerializer, AddDocumentSerializer


class DocumentViewSet(TypeDocumentViewSet, viewsets.ModelViewSet):
    '''
        Представления для Документов.
    '''
    queryset            = Document.objects.all()
    serializer_class    = DocumentSerializer
    model_class         = Document
    base_name           = 'document_document'
    
    def list(self, request):
        sql = MakeSQL(self.base_name)
        sql.add_order_by('-date')
        
        if ('date_start' in self.request.GET) and ('date_end' in self.request.GET):
            try:
                date_start = datetime.strptime(self.request.GET['date_start'], '%d-%m-%Y').date()
            except ValueError:
                raise ParseError(detail=u"Неверный формат начальной даты")
                
            try:
                date_end = datetime.strptime(self.request.GET['date_end'], '%d-%m-%Y').date()
            except ValueError:
                raise ParseError(detail=u"Неверный формат конечной даты")
            
            sql.add_filter(
                'date',
                (
                    date_start,
                    date_end,
                    ),
                'BETWEEN',
                )
            
        elif 'date_start' in self.request.GET:
            try:
                date_start = datetime.strptime(self.request.GET['date_start'], '%d-%m-%Y').date()
            except ValueError:
                raise ParseError(detail=u"Неверный формат начальной даты")
            
            sql.add_filter(
                'date',
                date_start,
                '>=',
                )
        
        elif 'date_end' in self.request.GET:
            try:
                date_end = datetime.strptime(self.request.GET['date_end'], '%d-%m-%Y').date()
            except ValueError:
                raise ParseError(detail=u"Неверный формат конечной даты")
            
            sql.add_filter(
                'date',
                date_end,
                '<=',
                )
        
        if 'type' in self.request.GET:
            try:
                pk = int(self.request.GET['type'])
            except ValueError:
                raise ParseError(detail=u"Неверный формат типа документа")
                
            _sql = MakeSQL('document_typedocument')
            _sql['fields'] = ['id',]
            _sql.add_filter('id', pk)
            _sql.add_limit(1)
            type_doc = TypeDocument.objects.raw(_sql.sql)
            
            try:
                type_doc = type_doc[0]
            except IndexError:
                raise NotFound(detail=u"Тип документа не существует")
                
            sql.add_filter('type_id', pk)
            
        if 'shop' in self.request.GET:
            try:
                shop = int(self.request.GET['shop'])
            except ValueError:
                raise ParseError(detail=u"Неверный формат магазина")
                
            _sql = MakeSQL('shop_shop')
            _sql['fields'] = ['id',]
            _sql.add_filter('id', shop)
            _sql.add_limit(1)
            shops = Shop.objects.raw(_sql.sql)
            
            try:
                shops = shops[0]
            except IndexError:
                raise NotFound(detail=u"Магазин не существует")
                
            sql.add_filter('shop_id', shop)
            
        if 'user' in self.request.GET:
            try:
                user = int(self.request.GET['user'])
            except ValueError:
                raise ParseError(detail=u"Неверный формат пользователя")
                
            _sql = MakeSQL('user_user')
            _sql['fields'] = ['id',]
            _sql.add_filter('id', user)
            _sql.add_limit(1)
            users = User.objects.raw(_sql.sql)
            
            try:
                users = users[0]
            except IndexError:
                raise NotFound(detail=u"Пользователь не существует")
                
            sql.add_filter('user_id', user)
            
        documents = self.model_class.objects.raw(sql.sql)
        return Response(
            self.get_serializer_class()(
                documents,
                many=True
                ).data
            )
            
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, user=request.user)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, user=request.user)
        return Response(serializer.data)

    def perform_update(self, serializer, user=None):
        if not user:
            serializer.save()
            
        serializer.save(user=user)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)
    
    def get_serializer_class(self, **kwargs):
        if self.action in ('update', 'partial_update', 'create'):
            return AddDocumentSerializer
            
        return super(DocumentViewSet, self).get_serializer_class(**kwargs)
        
    def check_object_permissions(self, request, obj, **kwargs):
        if self.action in ('update', 'partial_update', 'destroy'):
            if obj.type.blocking:
                if not request.user.has_perm('document.admin'):
                    raise PermissionDenied(detail=u"Разрешено только администраторам")
                        
        return super(DocumentViewSet, self).check_object_permissions(request, obj, **kwargs)
