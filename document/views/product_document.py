from rest_framework             import viewsets, status
from rest_framework.response    import Response
from rest_framework.exceptions  import PermissionDenied

from core.make_sql              import MakeSQL

from .type_document             import TypeDocumentViewSet

from ..models                   import ProductDocument
from ..serializers              import ProductDocumentSerializer, AddProductDocumentSerializer


class ProductDocumentViewSet(TypeDocumentViewSet, viewsets.ModelViewSet):
    '''
        Представление для промежуточной модели.
    '''
    queryset            = ProductDocument.objects.all()
    serializer_class    = ProductDocumentSerializer
    model_class         = ProductDocument
    base_name           = 'document_productdocument'
    
    def get_serializer_class(self, **kwargs):
        if self.action in ('update', 'partial_update', 'create'):
            return AddProductDocumentSerializer
            
        return super(ProductDocumentViewSet, self).get_serializer_class(**kwargs)
        
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, user=request.user)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        
    def perform_update(self, serializer, user):
        product = serializer.save()
        product.document.user = user
        product.document.save()
        
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        document = instance.document
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, user=request.user)
        
        return Response(serializer.data)
        
    def check_object_permissions(self, request, obj, **kwargs):
        if self.action in ('update', 'partial_update', 'destroy'):
            if obj.document.type.blocking:
                if not request.user.has_perm('document.admin'):
                    raise PermissionDenied(detail=u"Разрешено только администраторам")
                        
        return super(ProductDocumentViewSet, self).check_object_permissions(request, obj, **kwargs)
