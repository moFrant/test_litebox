from rest_framework             import viewsets
from rest_framework.response    import Response
from rest_framework.exceptions  import NotFound

from core.make_sql              import MakeSQL

from ..models                   import TypeDocument
from ..serializers              import TypeDocumentSerializer


class TypeDocumentViewSet(viewsets.ReadOnlyModelViewSet):
    '''
        Представления Типы документов.
    '''
    queryset            = TypeDocument.objects.all()
    serializer_class    = TypeDocumentSerializer
    model_class         = TypeDocument
    base_name           = 'document_typedocument'
    
    def list(self, request):
        sql             = MakeSQL(self.base_name)
        type_documents  = self.model_class.objects.raw(sql.sql)
        serializer      = self.get_serializer_class()(type_documents, many=True)
        
        return Response(serializer.data)
        
    def retrieve(self, request, pk=None):
        sql             = MakeSQL(self.base_name)
        sql.add_limit(1)
        
        try:
            _pk = int(pk)
        except:
            raise NotFount(detail=u"Объект не найден")
            
        sql.add_filter('id', _pk)
        
        type_documents  = self.model_class.objects.raw(sql.sql)[0]
        serializer      = self.get_serializer_class()(type_documents, many=False)
        
        return Response(serializer.data)
