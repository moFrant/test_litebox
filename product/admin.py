from core.models    import site, ModelAdmin

from .models        import UnitProduct, Product


class UnitProductAdmin(ModelAdmin):
    list_display        = ('name', 'abb')
    list_display_links  = ('name', 'abb')
    
class ProductAdmin(ModelAdmin):
    list_display        = ('name', 'unit', 'barcode')
    list_display_links  = ('name', 'unit', 'barcode')
    list_filter         = ('unit',)
    search_fields       = ['name', 'barcode']
    
site.register(UnitProduct, UnitProductAdmin)
site.register(Product, ProductAdmin)
