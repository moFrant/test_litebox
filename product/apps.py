from django.apps import AppConfig


class ProductConfig(AppConfig):
    name = 'product'
    label = 'product'
    verbose_name = u"Товары"
