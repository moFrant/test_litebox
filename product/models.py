'''
    Модель данных приложения Товары
'''
from core.models    import Model, models


class UnitProduct(Model):
    '''
        Модель для единиц измерения товаров.
    '''
    name    = models.CharField(
        max_length      = 50,
        verbose_name    = u"полное наименование",
        )
    
    abb     = models.CharField(
        max_length      = 5,
        verbose_name    = u"сокращенное наименование",
        )
        
    comment = models.TextField(
        verbose_name    = u"описание",
        blank           = True,
        )
        
    def __str__(self):
        return ' - '.join((
            self.abb,
            self.name,
            ))
            
    class Meta:
        verbose_name        = u"единица измерения"
        verbose_name_plural = u"единицы измерения"
    
    
class Product(Model):
    '''
        Модель для товаров.
    '''
    name    = models.CharField(
        max_length      = 50,
        verbose_name    = u"наименование",
        )
        
    unit    = models.ForeignKey(
        UnitProduct,
        verbose_name    = u"единица измерения",
        related_name    = "product_unit",
        blank           = True,
        null            = True,
        )
        
    barcode = models.CharField(
        max_length      = 16,
        verbose_name    = u"штрих код",
        blank           = True,
        )
        
    def __str__(self):
        return ', '.join((
            self.name,
            self.unit.abb,
            ))
        
    class Meta:
        verbose_name        = u"товар"
        verbose_name_plural = u"товары"
