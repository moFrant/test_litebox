'''
    Сериализаторы приложения Товары
'''
from rest_framework import serializers

from .models        import UnitProduct, Product


class UnitProductSerializer(serializers.ModelSerializer):
    '''
        Сериализатор для Единиц измерения.
    '''
    class Meta:
        model   = UnitProduct
        fields  = ('id', 'name', 'abb')
        
        
class UnitProductDetailSerializer(serializers.ModelSerializer):
    '''
        Подробный сериализатор для Единиц измерения.
    '''
    class Meta:
        model   = UnitProduct
        fields  = ('id', 'name', 'abb', 'comment')
        
        
class ProductSerializer(serializers.ModelSerializer):
    '''
        Сериализатор для Товара.
    '''
    unit = UnitProductSerializer(many=False, read_only=True)
    
    class Meta:
        model   = Product
        fields  = ('id', 'name', 'unit', 'barcode')
        

class AddProductSerializer(serializers.ModelSerializer):
    '''
        Сериализатор для добавления и обновления Товара.
    '''
    class Meta:
        model   = Product
        fields  = ('id', 'name', 'unit', 'barcode')
