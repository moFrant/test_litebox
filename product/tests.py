from django.test            import TestCase

from rest_framework.test    import APIClient
from rest_framework.reverse import reverse

from core.models            import models

from user.models            import User

from .models                import UnitProduct, Product
from .serializers           import UnitProductSerializer, UnitProductDetailSerializer, ProductSerializer
from .views                 import UnitViewSet, ProductViewSet

import json


class ProductTestCase(TestCase):
    '''
        Тестирование приложения Товары
    '''
    def setUp(self):
        self.APIClient = APIClient()
        
        unit = UnitProduct(
            name    = u"штука",
            abb     = u"шт.",
            comment = u"используется для товаров, которые можно посчитать штуками"
            )
            
        unit.save()
        
        product = Product(
            name    = u"карандаш",
            unit    = unit,
            barcode = 'qwertyuiop',
            )
            
        product.save()
        
        user = User(
            username    = 'user',
            first_name  = u'Иван',
            patronymic  = u'Иванович',
            last_name   = u'Иванов',
            email       = u'd@wview.ru',
            password    = 'Test0001',
            )
        
        user.save()
        
    def test_models(self):
        '''
            Проверка моделей данных.
        '''
        unit    = UnitProduct.objects.all()[0]
        product = Product.objects.all()[0]
        
        self.assertTrue(type(unit._meta.get_field('name')) is models.CharField)
        self.assertTrue(type(unit._meta.get_field('abb')) is models.CharField)
        self.assertTrue(type(unit._meta.get_field('comment')) is models.TextField)
        
        self.assertTrue(type(product._meta.get_field('name')) is models.CharField)
        self.assertTrue(type(product._meta.get_field('unit')) is models.ForeignKey)
        self.assertTrue(product._meta.get_field('unit').remote_field.model is UnitProduct)
        self.assertEqual(product._meta.get_field('unit').remote_field.related_name, "product_unit")
        self.assertEqual(product._meta.get_field('unit').blank, True)
        self.assertEqual(product._meta.get_field('unit').null, True)
        self.assertTrue(type(product._meta.get_field('barcode')) is models.CharField)
        
    def test_serialisers(self):
        '''
            Тестирование сериализаторов.
        '''
        unit    = UnitProduct.objects.all()[0]
        product = Product.objects.all()[0]
        
        unit_s      = {
            'id':   unit.pk,
            'name': unit.name,
            'abb':  unit.abb,
            }
            
        unit_detail = {
            'id':       unit.pk,
            'name':     unit.name,
            'abb':      unit.abb,
            'comment':  unit.comment,
            }
            
        product_s   = {
            'id':       product.pk,
            'name':     product.name,
            'unit':     {
                'id':   product.unit.pk,
                'name': product.unit.name,
                'abb':  product.unit.abb,
                },
            'barcode':  product.barcode
            }
            
        self.assertEqual(UnitProductSerializer(unit, many=False).data, unit_s)
        self.assertEqual(UnitProductDetailSerializer(unit, many=False).data, unit_detail)
        self.assertEqual(ProductSerializer(product, many=False).data, product_s)
        
    def test_unit_views(self):
        '''
            Проверка представления Единицы измерения.
        '''
        unit = UnitProduct.objects.all()[0]
        
        unit_s      = {
            'id':   unit.pk,
            'name': unit.name,
            'abb':  unit.abb,
            }
            
        unit_detail = {
            'id':       unit.pk,
            'name':     unit.name,
            'abb':      unit.abb,
            'comment':  unit.comment,
            }
        
        self.APIClient.login(username='user', password='Test0001')
        
        response = self.APIClient.get(reverse('unitproduct-list'))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(unit_s in response_json['results'])
        
        response = self.APIClient.get(reverse('unitproduct-detail', args=[unit.pk]))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(unit_detail == response_json)
        
    def test_product_views(self):
        '''
            Проверка представления Товар.
        '''
        product = Product.objects.all()[0]
        
        product_s = {
            'id':       product.pk,
            'name':     product.name,
            'unit':     {
                'id':   product.unit.pk,
                'name': product.unit.name,
                'abb':  product.unit.abb,
                },
            'barcode':  product.barcode
            }
            
        self.APIClient.login(username='user', password='Test0001')
        
        response = self.APIClient.get(reverse('product-list'))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(product_s in response_json['results'])
        
        response = self.APIClient.get(reverse('product-detail', args=[product.pk]))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(product_s == response_json)
        
        unit = UnitProduct.objects.all()[0]
        
        response = self.APIClient.post(
            reverse('product-list'), 
            {
                'name': u'Табурет',
                'unit': unit.pk,
                },
            format = 'json',
            )
        
        self.assertEqual(response.status_code, 201)
        
        product_new = Product.objects.filter(name=u"Табурет")
        self.assertTrue(product_new)
        
        response = self.APIClient.patch(
            reverse('product-detail', args=[product_new[0].pk]),
            {
                'barcode': 'poiuytrewq',
                },
            format = 'json',
            )
            
        product_new = Product.objects.filter(name=u"Табурет")
        self.assertEqual(product_new[0].barcode, 'poiuytrewq')
        
        response = self.APIClient.get(
            reverse('product-list'),
            {
                'search': 'аранд',
                },
            )
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(product_s in response_json['results'])
        
        response = self.APIClient.get(
            reverse('product-list'),
            {
                'search': 'poiuytrew',
                },
            )
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(len(response_json['results']) == 0)
        
        response = self.APIClient.get(
            reverse('product-list'),
            {
                'search': 'poiuytrewq',
                },
            )
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(len(response_json['results']) == 1)
        
        response = self.APIClient.get(
            reverse('product-list'),
            {
                'unit': unit.pk,
                },
            )
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(len(response_json['results']) == 2)
        
        response = self.APIClient.delete(reverse('product-detail', args = [product_new[0].pk]))
        
        product_new = Product.objects.filter(name=u"Табурет")
        self.assertFalse(product_new)
