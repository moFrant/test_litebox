from rest_framework.routers import DefaultRouter

from .views                 import UnitViewSet, ProductViewSet

router = DefaultRouter()
router.register(r'units', UnitViewSet)
router.register('products', ProductViewSet)

urlpatterns = router.urls
