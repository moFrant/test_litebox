from rest_framework import viewsets, filters

from ..models       import Product
from ..serializers  import ProductSerializer, AddProductSerializer

import django_filters


class ProductUnitFilter(django_filters.FilterSet):
    '''
        Фильтр для поиска по единицам измерения.
    '''
    unit = django_filters.NumberFilter(name="unit__pk", distinct=True)
    
    class Meta:
        model = Product
        fields = ['unit',]


class ProductViewSet(viewsets.ModelViewSet):
    '''
        Представления для продукта.
    '''
    queryset            = Product.objects.all()
    serializer_class    = ProductSerializer
    filter_backends     = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields       = ('name', '=barcode')
    filter_class        = ProductUnitFilter
    
    def get_serializer_class(self, **kwargs):
        if self.action in ('update', 'partial_update', 'create'):
            return AddProductSerializer
            
        return super(ProductViewSet, self).get_serializer_class(**kwargs)
