from rest_framework import viewsets

from ..models       import UnitProduct
from ..serializers  import UnitProductSerializer, UnitProductDetailSerializer


class UnitViewSet(viewsets.ReadOnlyModelViewSet):
    '''
        Представления для модели Единицы измерения
    '''
    queryset    = UnitProduct.objects.all()
    
    def get_serializer_class(self, **kwargs):
        if self.action == 'retrieve':
            return UnitProductDetailSerializer
        
        else:
            return UnitProductSerializer
