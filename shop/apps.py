from django.apps import AppConfig


class ShopConfig(AppConfig):
    name = 'shop'
    label = 'shop'
    verbose_name = u"Магазины"
