'''
    Модели данных приложения Магазины.
'''
from core.models import Model, models


class Shop(Model):
    '''
        Модель данных Магазин.
    '''
    name = models.CharField(
        max_length      = 50,
        verbose_name    = u"название магазина",
        )
        
    def __str__(self):
        return self.name
        
    class Meta:
        verbose_name        = u"магазин"
        verbose_name_plural = u"магазины"
