'''
    Сериализаторы приложения Магазины.
'''
from rest_framework import serializers

from .models        import Shop


class ShopSerializer(serializers.ModelSerializer):
    '''
        Сериализатор для Магазинов.
    '''
    class Meta:
        model   = Shop
        fields  = ('id', 'name')
