from django.test            import TestCase

from rest_framework.test    import APIClient
from rest_framework.reverse import reverse

from core.models            import models

from user.models            import User

from .models                import Shop
from .serializers           import ShopSerializer
from .views                 import ShopViewSet

import json


class ShopTestCase(TestCase):
    '''
        Тестирование приложение Магазины.
    '''
    def setUp(self):
        self.APIClient = APIClient()
        
        shop = Shop(
            name = u"тестовый магазин",
            )
            
        shop.save()
        
        user = User(
            username    = 'user',
            first_name  = u'Иван',
            patronymic  = u'Иванович',
            last_name   = u'Иванов',
            email       = u'd@wview.ru',
            password    = 'Test0001',
            )
        
        user.save()
        
    def test_models(self):
        '''
            Проверка моделей данных.
        '''
        shop = Shop.objects.all()[0]
        
        self.assertTrue(type(shop._meta.get_field('name')) is models.CharField)
        
    def test_serialiser(self):
        '''
            Проверка сериализаторов
        '''
        shop = Shop.objects.all()[0]
        
        shop_s = {
            'id':   shop.pk,
            'name': shop.name,
            }
            
        self.assertEqual(ShopSerializer(shop, many=False).data, shop_s)
        
    def test_shop_views(self):
        '''
            Проверка представлений Магазина.
        '''
        shop = Shop.objects.all()[0]
        
        shop_s = {
            'id':   shop.pk,
            'name': shop.name,
            }
        
        self.APIClient.login(username='user', password='Test0001')
        
        response = self.APIClient.get(reverse('shop-list'))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(shop_s in response_json['results'])
        
        response = self.APIClient.get(reverse('shop-detail', args=[shop.pk]))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(shop_s == response_json)
        
        response = self.APIClient.get(reverse('shop-list'), {'search': u'Тест'})
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(shop_s in response_json['results'])
