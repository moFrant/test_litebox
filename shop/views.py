'''
    Представления для REST API
'''
from rest_framework import viewsets, filters

from .models        import Shop
from .serializers   import ShopSerializer


class ShopViewSet(viewsets.ReadOnlyModelViewSet):
    '''
        Представления для модели Магазины
    '''
    queryset            = Shop.objects.all()
    filter_backends     = (filters.SearchFilter,)
    search_fields       = ('name',)
    serializer_class    = ShopSerializer
