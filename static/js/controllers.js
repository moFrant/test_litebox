myApp.controller('MainController', function($scope, $http, $filter) {
    $scope.all_document = function () {
        $http.get(api_urls['document']).then(function(response) {
            $scope.documents = response.data;
            $scope.count = $scope.documents.length;
            });
        };
        
    $scope.all_document();
    
    $http.get(api_urls['type_document']).then(function(response) {
        $scope.types = response.data;
        });
        
    $http.get(api_urls['shop']).then(function(response) {
        $scope.shops = response.data['results'];
        });
        
    $http.get(api_urls['user']).then(function(response) {
        $scope.users = response.data['results'];
        });
    
    $scope.new_products = [{},];
    
    $http.get(api_urls['product']).then(function(response) {
        $scope.products = response.data['results'];
        });
    
    $scope.del_field = function(obj) {
        index = $scope.new_products.indexOf(obj);
        $scope.new_products.splice(index, 1);
        };
    
    $scope.add_field = function() {
        $scope.new_products.push({});
        };
    
    $scope.add_document_cancel = function() {
        delete $scope.one_document;
        $scope.new_products = [{},];
        $scope.new = false;
        $scope.view = false;
        };
    
    $scope.add_document = function() {
        $scope.one_document.date = $filter('date')($scope.one_document.date, 'yyyy-MM-dd');
        $http.post(api_urls['document'], $scope.one_document).then(function(response) {
            $scope.new_products.forEach(function(product) {
                $http.post(api_urls['product_document'], {'product': product.product.id, 'count': product.count, 'document': response.data.id})
                });
            $scope.add_document_cancel();
            $scope.all_document();
            });
        };
        
    $scope.new_obj = function() {
        $scope.new = true;
        $scope.one_document = {};
        $scope.new_products = [{},];
        }
    
    $scope.view = false;
    
    $scope.showView = function(obj) {
        $scope.view = true;
        $scope.new = false;
        $scope.one_document = {};
        $scope.one_document.date = new Date(obj.date);
        $scope.one_document.type = obj.type.id;
        $scope.one_document.shop = obj.shop.id;
        $scope.one_document.user = obj.user;
        $scope.one_document.id = obj.id;
        $scope.new_products = obj.product;
        };
        
    $scope.edit_document = function() {
        $scope.one_document.date = $filter('date')($scope.one_document.date, 'yyyy-MM-dd');
        $http.patch(api_urls['document'] + $scope.one_document.id + '/', $scope.one_document).then(function(response) {
            $scope.new_products.forEach(function(product) {
                if (product.id) {
                    $http.patch(api_urls['product_document'] + product.id + '/', {'product': product.product.id, 'count': product.count, 'document': response.data.id});
                    } else {
                        $http.post(api_urls['product_document'], {'product': product.product.id, 'count': product.count, 'document': response.data.id});
                        };
                });
                
            $scope.add_document_cancel();
            $scope.all_document();
            })
            
            .catch(function(data) {
                alert(data.data.detail);
                });
        };
        
    $scope.delete_document = function() {
        $http.delete(api_urls['document'] + $scope.one_document.id + '/').then(function() {
            $scope.add_document_cancel();
            $scope.all_document();
            })
            
            .catch(function(data) {
                alert(data.data.detail);
                });;
        }
        
    $scope.add_filter = function() {
        if ($scope.filter.date_start) {
            $scope.filter.date_start = $filter('date')($scope.filter.date_start, 'dd-MM-yyyy');
            };
            
        if ($scope.filter.date_end) {
            $scope.filter.date_end = $filter('date')($scope.filter.date_end, 'dd-MM-yyyy');
            };
            
        $http.get(api_urls['document'], {params: $scope.filter}).then(function(response) {
            $scope.documents = response.data;
            $scope.count = $scope.documents.length;
            });
        };
        
    $scope.del_filter = function() {
        $scope.filter = {};
        $scope.all_document();
        };
    
    });
    
    
    
    
myApp.controller('ProductController', function($scope, $http) {
    $scope.all_product = function() {
        $http.get(api_urls['product']).then( function(response) {
            $scope.products = response.data['results'];
            $scope.counts = response.data['count'];
            $scope.next = response.data['next'];
            $scope.previous = response.data['previous'];
            });
        $scope.newrecord = {};
        $scope.edit = false;
        };
    
    $http.get(api_urls['unit_product']).then( function(response) {
        $scope.units = response.data['results'];
        });
    
    $scope.all_product();
    
    $scope.filter_product = function() {
        param = '';
        if ($scope.unitFilter) {
            param += 'unit='+$scope.unitFilter.id;
            };
        
        if ($scope.search) {
            if (param) {
                param += '&';
                }
            param += 'search='+$scope.search;
            };
        
        $http.get(api_urls['product']+'?'+ param).then( function(response) {
            $scope.products = response.data['results'];
            $scope.counts = response.data['count'];
            });
        };
        
    $scope.delete_product = function(id) {
        $http.delete(api_urls['product'] + id + '/').then( function(response) {
            $scope.all_product();
            });
        };
        
    $scope.add_product = function() {
        $http.post(api_urls['product'], $scope.newrecord).then(function(response) {
            $scope.all_product();
            });
        };
        
    $scope.edit_product = function(id) {
        $scope.newrecord = id;
        $scope.newrecord.unit = id.unit.id;
        $scope.products = [];
        $scope.edit = true;
        };
        
    $scope.edit_product_req = function() {
        $http.patch(api_urls['product'] + $scope.newrecord.id + '/', $scope.newrecord).then(function(response) {
            $scope.all_product();
            });
            
        };
    });
