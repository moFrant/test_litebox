myApp.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    $routeProvider.when("/", {
        templateUrl: '/static/templates/main.htm',
        controller: 'MainController'
        });
    $routeProvider.when("/products", {
        templateUrl: '/static/templates/products.htm',
        controller: 'ProductController'
        });
    });
