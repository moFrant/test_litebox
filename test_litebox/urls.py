"""test_litebox URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls   import url, include

from core.models        import site

from user.views         import LoginFormView, logout_url
from views              import MainView

urlpatterns = [
    url(r'^admin/', site.urls),
    
    url(r'^$', MainView.as_view()),
    
    url(r'^login/$', LoginFormView.as_view()),
    url(r'^logout/$', logout_url),
    
    url(r'^users/', include('user.urls')),
    url(r'^products/', include('product.urls')),
    url(r'^shops/', include('shop.urls')),
    url(r'^documents/', include('document.urls')),
]
