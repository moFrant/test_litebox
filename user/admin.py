from core.models        import ModelAdmin, site

from .models            import User, VerifyUser


class UserAdmin(ModelAdmin):
    list_display        = ('username', 'first_name', 'patronymic', 'last_name', 'is_active', 'is_staff')
    list_display_links  = ('username', 'first_name', 'patronymic', 'last_name', 'is_active', 'is_staff')
    list_filter         = ('is_active', 'is_staff')
    actions             = ['set_password']
    readonly_fields     = ('is_superuser', 'user_permissions', 'last_login', 'date_joined')
    
    fieldsets = (
        (None, {
            'fields': ('username', 'password'),
            }),
        (u'Персональные данные', {
            'classes': ('wide',),
            'fields': ('first_name', 'patronymic', 'last_name', 'email'),
            }),
        (u'Администрирование', {
            'classes': ('collapse',),
            'fields': ('is_active', 'is_staff', 'groups'),
            }),
        (u'Другое', {
            'classes': ('collapse',),
            'fields': ('is_superuser', 'last_login', 'date_joined'),
            })
        )
    
    def get_changeform_initial_data(self, request):
        return {
            'is_active': False,
            }
            
    def set_password(self, request, queryset):
        '''
            Отправляет на почту уведомление о сбросе/установке пароля.
            После того, как пароль будет установлен, учетная запись станет активной.
        '''
        i = 0
        for user in queryset:
            user.is_active = False
            
            user.save()
            
            v = VerifyUser(
                user = user,
                )
                
            v.save()
            
            i += 1
            
        self.message_user(request, "%s записей обработано. Отправлены ссылки для установки нового пароля." % i)
        
    set_password.short_description = "Сброс/Установка пароля"
    
    
class VerifyUserAdmin(ModelAdmin):
    list_filter = ('user',)
    
site.register(User, UserAdmin)

site.register(VerifyUser, VerifyUserAdmin)
