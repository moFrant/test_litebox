from django.shortcuts   import redirect
from django.conf        import settings


def authorization(function):
    '''
        Декоратор, проверяет авторизован ли пользователь.
    '''
    def _run(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect("/login/")
            
        return function(self, request, *args, **kwargs)
        
    return _run
