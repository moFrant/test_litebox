from django.db                      import models
from django.contrib.auth.models     import AbstractUser, UserManager
from django.db.models.signals       import pre_save, post_save
from django.dispatch                import receiver
from django.contrib.auth.hashers    import check_password, make_password, is_password_usable
from django.core.mail               import send_mail

from core.models                    import Model

from test_litebox.settings          import get_config


class User(AbstractUser):
    '''
        Модель данных Пользователь.
        
        Отличия от стандартной модели данных - наличие дополнительного поля "Отчество"
    '''
    patronymic  = models.CharField(
        max_length      = 50,
        verbose_name    = u"отчество",
        blank           = True,
        )
        
    def __str__(self):
        return ' '.join((
            self.first_name,
            self.patronymic,
            self.last_name,
            ))
    
    class Meta:
        verbose_name        = u"пользователь"
        verbose_name_plural = u"пользователи"
        
        
@receiver(pre_save, sender=User)
def save_password(sender, **kwargs):
    '''
        Проверяет изменение модели, если происходит изменение пароля, то в базу заносится хэш.
    '''
    old_password = None
    user = kwargs['instance']
    
    if not is_password_usable(user.password):
    
        try:
            old_password = User.objects.get(pk=user.pk).password
        except:
            pass
    
        if old_password:
            if not check_password(user.password, old_password) and old_password != user.password:
                user.password = make_password(user.password)
        else:
            user.password = make_password(user.password)


def generation_string():
    um = UserManager()
    return(um.make_random_password(length=25))
            
            
class VerifyUser(Model):
    '''
        Модель данных для запросов подтверждения email.
    '''
    user    = models.ForeignKey(
        User,
        verbose_name    = u"пользователь",
        related_name    = "user_verifyuser",
        )
        
    string  = models.CharField(
        verbose_name    = u"уникальная стока",
        unique          = True,
        default         = generation_string,
        max_length      = 25,
        )
        
    used    = models.BooleanField(
        verbose_name    = u"использованный запрос",
        default         = False,
        )
        
    def __str__(self):
        return str(self.user)
        
    class Meta:
        verbose_name        = u"запрос"
        verbose_name_plural = u"запросы"
        
        
@receiver(post_save, sender=VerifyUser)
def send_notification(sender, **kwargs):
    '''
        Сигнал после сохранения объекта VerifyUser. Отправляет письмо на почту со ссылкой, для установки/смены пароля
    '''
    verify = kwargs['instance']
    
    if verify.used:
        verify.delete()
        
    else:
        if get_config("SEND_MAIL") and verify.user.email:
            if (not verify.user.is_active) and verify.user.email:
                text = "Для подтверждения Вашей учетной записи перейдите по ссылке " + get_config("URL_ROOT") + "/users/changepassword/" + verify.string + '/'
                
                send_mail('Подтверждение учетной записи', text, get_config("DEFAULT_FROM_EMAIL"), [verify.user.email,])
