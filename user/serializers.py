'''
    Сериализаторы приложения Пользователь.
'''
from rest_framework import serializers

from .models        import User


class UserSerializer(serializers.ModelSerializer):
    '''
        Сериализатор модели Пользователь.
    '''
    class Meta:
        model   = User
        fields  = ('id', 'username', 'first_name', 'patronymic', 'last_name')
