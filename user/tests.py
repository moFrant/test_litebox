'''
    Модуль с Unit-тестами.
'''
from django.test                import TestCase
from django.core                import mail
from django.contrib.auth.forms  import SetPasswordForm

from rest_framework.test        import APIClient
from rest_framework.reverse     import reverse

from .models                    import User, VerifyUser
from .serializers               import UserSerializer

import re
import json


class UserTest(TestCase):
    def setUp(self):
        self.APIClient = APIClient()
        
        #создание обычного пользователя
        user = User(
            username    = 'user',
            first_name  = u'Иван',
            patronymic  = u'Иванович',
            last_name   = u'Иванов',
            email       = u'd@wview.ru',
            password    = 'Test0001',
            )
        
        user.save()
        
        #создание администратора
        user = User(
            username    = 'admin',
            first_name  = u'Олег',
            patronymic  = u'Петрович',
            last_name   = u'Бабушкин',
            email       = u'mofrant@wview.ru',
            is_staff    = True,
            password    = 'Test0002',
            )
        
        user.save()
        
    def test_models(self):
        '''
            Тест, проверяющий корректность модели.
        '''
        user = User.objects.all()
        
        p_label     = user[0]._meta.get_field('patronymic').verbose_name
        p_length    = user[0]._meta.get_field('patronymic').max_length
        p_blank     = user[0]._meta.get_field('patronymic').blank
        
        self.assertEquals(p_label, u'отчество')
        self.assertEquals(p_length, 50)
        self.assertEquals(p_blank, True)
        
    def test_save_password(self):
        '''
            Тест, проверяющий правильность сохранения пароля.
        '''
        user = User.objects.all()[1]
        
        user.password = 'TestUser1'
        
        user.save()
        
        user = User.objects.get(pk=user.pk)
        
        self.assertTrue(user.check_password('TestUser1'))
        
    def test_reset_password(self):
        '''
            Тест, проверяющий отправку электронного письма со ссылкой для создания пароля.
        '''
        user            = User.objects.all()[0]
        user.is_active  = False
        user.save()
        
        verify  = VerifyUser(
            user = user,
            )
            
        verify.save()
        
        self.assertEqual(mail.outbox[0].subject, u'Подтверждение учетной записи')
        self.assertTrue(user.email in mail.outbox[0].to)
        
        pattern = re.compile('(https?\:\/\/(\w+\.?\w+?)+(\:\d+)?\/\w+\/\w+\/\w+\/)')
        self.assertTrue(pattern.findall(mail.outbox[0].body))
        
        url         = pattern.findall(mail.outbox[0].body)[0][0]
        response    = self.client.get(url)
        self.assertTrue(type(response.context['form']) is SetPasswordForm)
        self.assertTemplateUsed(response, 'change_password.htm')
        
        response    = self.client.post(
            url,
            {
                'new_password1': 'asdWdfg4ws3d',
                'new_password2': 'asdWdfg4ws3d',
            })
            
        self.assertEqual(response.status_code, 302)
            
        user = User.objects.get(pk=user.pk)
        self.assertTrue(user.check_password('asdWdfg4ws3d'))
        
    def test_login_page(self):
        '''
            Тест проверки страницы входа
        '''
        response = self.client.get(
            '/login/', 
            {
                'way': '/',
            })
        self.assertTemplateUsed(response, 'login.htm')
        
    def test_serializers(self):
        '''
            Проверка сериализаторов приложения.
        '''
        user = User.objects.all()[0]
        
        user_serializer = {
            'id':           user.pk,
            'username':     user.username,
            'first_name':   user.first_name,
            'patronymic':   user.patronymic,
            'last_name':    user.last_name,
            }
            
        self.assertEqual(user_serializer, UserSerializer(user, many=False).data)
        
    def test_api_view(self):
        user = User.objects.all()[0]
        
        user_serializer = {
            'id':           user.pk,
            'username':     user.username,
            'first_name':   user.first_name,
            'patronymic':   user.patronymic,
            'last_name':    user.last_name,
            }
            
        self.APIClient.login(username='user', password='Test0001')
        
        response = self.APIClient.get(reverse('user-list'))
        self.assertEqual(response.status_code, 200)
        
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertTrue(user_serializer in response_json['results'])
