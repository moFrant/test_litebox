from django.conf.urls       import url

from rest_framework.routers import DefaultRouter

from .views                 import ResetPassword, UserViewSet


urlpatterns = [
    url(r'^changepassword/(?P<string>\w{25})/$', ResetPassword.as_view()),
    ]
    

router = DefaultRouter()
router.register('users', UserViewSet)

urlpatterns += router.urls
