from django.views.generic.edit  import FormView
from django.contrib.auth.forms  import AuthenticationForm
from django.contrib.auth        import login

from datetime                   import datetime, timedelta


class LoginFormView(FormView):
    '''
        Класс представления страницы авторизации.
        
        :param class form_class: Класс формы авторизации.
        :param str template_name: Название используемого шаблона.
        :param str success_url: Адрес перенаправления в случае успешной авторизации.
        :param int count_invalid: Количество неправильных вводов пароля за 1 час.
    '''
    form_class      = AuthenticationForm
    template_name   = "login.htm"
    success_url     = "/"
    count_invalid   = 3


    def get_success_url(self):
        '''
            Изменение адреса перенаправления. Адрес будет браться из GET параметров.
        '''
        if 'way' in self.request.GET:
            way = self.request.GET['way']
            
            LoginFormView.success_url = way
            
        return super(LoginFormView, self).get_success_url()


    def form_valid(self, form):
        '''
            Метод, при успешном заполнении формы
        '''
        self.user = form.get_user()

        login(self.request, self.user)
        
        if 'invalid_auth' in self.request.session:
            del self.request.session['invalid_auth']
        
        return super(LoginFormView, self).form_valid(form)
        
        
    def form_invalid(self, form):
        '''
            Метод при неверном вводе пароля. В нем реализован механизм защиты.
        '''
        if 'invalid_auth' in self.request.session:
            date        = datetime.strptime(
                            self.request.session['invalid_auth'][0],
                            "%Y.%m.%d %H:%M:%S"
                            )
            date_delta  = date + timedelta(hours=1)
            
            if datetime.now() >= date_delta:
                self.request.session['invalid_auth'] = [
                    datetime.now().strftime("%Y.%m.%d %H:%M:%S"),
                    1,
                    ]
                    
            else:
                count = self.request.session['invalid_auth'][:]
                count[1] += 1
                self.request.session['invalid_auth'] = count
        
        else:
            self.request.session['invalid_auth'] = [
                datetime.now().strftime("%Y.%m.%d %H:%M:%S"),
                1,
                ]
            
        return super(LoginFormView, self).form_invalid(form)
        
        
    def get_context_data(self, **kwargs):
        '''
            Переопределение родительского метода, чтоб скрывать форму при привышении лимита ввода неправильных паролей.
        '''
        
        context = super(LoginFormView, self).get_context_data(**kwargs)
        
        context['sec_auth'] = False
        
        if 'invalid_auth' in self.request.session:
            date        = datetime.strptime(
                                self.request.session['invalid_auth'][0],
                                "%Y.%m.%d %H:%M:%S"
                                )
            date_delta  = date + timedelta(hours=1)
            
            if self.request.session['invalid_auth'][1] >= self.count_invalid and datetime.now() < date_delta:
                context['sec_auth'] = True
            
        return context
