from django.contrib.auth import logout
from django.shortcuts import redirect

def logout_url(request):
    logout(request)
    return redirect('/')
