from django.contrib.auth.forms  import SetPasswordForm
from django.shortcuts           import redirect
from django.views.generic.base  import TemplateView

from ..models                   import VerifyUser


class ResetPassword(TemplateView):
    '''
        Представление для сброса пароля и подтверждения учетной записи.
    '''
    template_name       = 'change_password.htm'
    http_method_names   = ['get', 'post']
    banner              = False
    name_page           = None
    title               = 'Установить пароль для учетной записи'
    del_region          = False
    form_data           = False
    
    def post(self, request, *args, **kwargs):
        try:
            verify = VerifyUser.objects.get(
                string  = kwargs['string'],
                used    = False,
                )
            
        except:
            verify = None
            
        if verify:
            form = SetPasswordForm(verify.user, request.POST)
            
            if form.is_valid():
                user            = form.save()
                user.is_active  = True
                user.save()
                verify.used     = True
                verify.save()
                
                return redirect('/')
                
            else:
                self.form = form
                self.form_data = True
                
        return super(ResetPassword, self).get(request, *args, **kwargs)
        
        
    def get_context_data(self, **kwargs):
        context = super(ResetPassword, self).get_context_data(**kwargs)
        
        try:
            verify = VerifyUser.objects.get(
                string  = kwargs['string'],
                used    = False,
                )
                
            context['verify'] = True
            
            if self.form_data:
                context['form'] = self.form
                
            else:
                context['form'] = SetPasswordForm(verify.user)
                
        except:
            context['verify'] = False
            
        return context
