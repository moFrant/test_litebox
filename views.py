from django.views.generic.base      import TemplateView
from rest_framework.reverse         import reverse

from user.decorations               import authorization

import json


class MainView(TemplateView):
    template_name       = 'base.htm'
    http_method_names   = ['get',]
    
    @authorization
    def get(self, request, *args, **kwargs):
        return super(MainView, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        
        urls = {
            'unit_product':     reverse('unitproduct-list'),
            'product':          reverse('product-list'),
            'type_document':    reverse('typedocument-list'),
            'product_document': reverse('productdocument-list'),
            'document':         reverse('document-list'),
            'shop':             reverse('shop-list'),
            'user':             reverse('user-list'),
            }
        
        context['urls_api'] = json.dumps(urls)
        
        return context
